import functionHelper from "~/libraries/formatHelpers/functionHelper";
import type IBase from "./IBase";
interface IPost extends IBase {
	cat_id: number,
	category: Record<string, undefined | null>,
	title: string,
	address: string,
	describe: string,
	images: [],
	remove_images: [],
	view: number,
	embed_link: string,
	uuid: string,
	price: number,
	like: number,
	cmt: number,
	place_type: string,
}
class Post implements IPost {
	address!: string;
	cat_id!: number;
	category!: Record<string, undefined | null>;
	created_at: string;
	describe!: string;
	id!: number;
	images!: [];
	is_delete!: boolean;
	is_visible!: boolean;
	embed_link!: string;
	remove_images!: [];
	sort!: number;
	title!: string;
	updated_at: string;
	uuid!: string;
	view!: number;
	price!: number;
	like!: number;
	cmt!: number;
	place_type!: string;
	get getMapDisplay () {
		return functionHelper.getMapDisplay(this.embed_link);
	}
	constructor(init: IPost) {
		Object.assign(this, init);
		this.created_at = functionHelper.dateStringTo12Hour(init.created_at);
		this.updated_at = functionHelper.dateStringTo12Hour(init.updated_at);
		this.images = !init.images ? [] : init.images
	}
}
interface IBlogArticle extends IBase {
	cat_id: number,
	title: string,
	describe: string,
	view: number,
	image: string,
	uuid: string,
	post_date: string,
	embed_link: string,
}
class BlogArticle implements IBlogArticle {
	id!: number;
	cat_id!: number;
	is_delete!: boolean;
	is_visible!: boolean;
	embed_link!: string
	title!: string;
	describe!: string;
	post_date!: string;
	image!: string;
	sort!: number;
	uuid!: string;
	view!: number;
	created_at: string;
	updated_at: string;
	get getMapDisplay () {
		return functionHelper.getMapDisplay(this.embed_link);
	}
	constructor(init: IBlogArticle) {
		Object.assign(this, init);
		this.created_at = functionHelper.dateStringTo12Hour(init.created_at)
		this.updated_at = functionHelper.dateStringTo12Hour(init.updated_at);
	}
}
interface ISubComm {
	id: number,
	p_id: number,
	post_id: number,
	comment: string,
	subComments: [],
	user: {
		userName: string,
		profile: string,
	},
	created_at: string,
	updated_at: string,
}
interface IComment {
	id: number,
	p_id: number,
	post_id: number,
	comment: string,
	// subComments: ISubComm[],
	user: {
		userName: string,
		profile: string,
	},
	created_at: string,
	updated_at: string,
}
interface IDetailPost extends IPost {
	comments: IComment[],
	user: {
		userName: string,
		profile: string,
	},

	created_at : string,
	updated_at : string,
}
class DetailPost implements IDetailPost {
	address!: string;
	cat_id!: number;
	category!: Record<string, undefined | null>;
	created_at!: string;
	describe!: string;
	id!: number;
	images!: [];
	is_delete!: boolean;
	is_visible!: boolean;
	embed_link!: string;
	remove_images!: [];
	sort!: number;
	title!: string;
	updated_at!: string;
	uuid!: string;
	view!: number;
	price!: number;
	comments: IComment[];
	like!: number;
	cmt!: number;
	place_type!: string;
	user!: { userName: string; profile: string; };
	get getMapDisplay () {
		return functionHelper.getMapDisplay(this.embed_link);
	}
	constructor(init: IDetailPost) {
		Object.assign(this, init);
		this.created_at = functionHelper.dateStringTo12Hour(init.created_at);
		this.updated_at = functionHelper.dateStringTo12Hour(init.updated_at);
		this.comments = init.comments.map((comment: any) => ({
			id: comment.id,
			p_id: comment.p_id,
			post_id: comment.post_id,
			comment: comment.comment,
			user: {
				userName: comment.user.userName,
				profile: comment.user.profile,
			},
			created_at: functionHelper.dateStringTo12HourWithTime(comment.created_at),
			updated_at: functionHelper.dateStringTo12HourWithTime((comment.updated_at)),
		}))
	}
}
export {
	type IPost,
	type IBlogArticle,
	Post, BlogArticle, DetailPost,
	type IDetailPost,
}