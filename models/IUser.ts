interface IUserInfos {
    userName: string,
    profile: string,
    phone: string,
}
interface IAuth {
    access_token: string,
    expires_in: number,
    userInfo: IUserInfos,
    accessToken: string,

}
export {
    type IAuth,
    type IUserInfos,
}