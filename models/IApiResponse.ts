export default interface ApiResponse {
  code: number;
  data: []; // Adjust the type of data based on your actual data structure
  message: string;
}