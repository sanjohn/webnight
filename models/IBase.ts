export default interface IBase {
    id: number,
    sort: number,
    is_visible: boolean,
    created_at: string,
    updated_at: string
    is_delete: boolean,
}