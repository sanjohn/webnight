import { defineStore } from 'pinia';
import type { ICategory } from '~/models/ICategory';
import type { IUserInfos } from "~/models/IUser";

export const useStore = defineStore('store', {
    state: () => {
        return {
            lang: 'kh',
            ip: '',
            userInfos: {
                userName: '',
                profile: '',
                phone: '',
            },
            accessToken: '',
            onSearch: false,
            onMobileDialog: false,
            categories: <ICategory[]>[],
            themeColor: '#818CF8',
        }
    },
    actions: {
        updateLocale(lang: string) {
            this.lang = lang;
        },
        updateUserInfo(info: IUserInfos) {
            this.userInfos = info;
        },
        setCategories(categories: any) {
            this.categories = categories;
        },
        setThemeColor(primary: string) {
            this.themeColor = primary;
        }
    },
    getters: {
        isAuth(state) {
            return !!(state.userInfos.userName && Session.get('token'));
        }
    },
    persist: {
        storage: persistedState.localStorage,
    },
})
