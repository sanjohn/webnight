import kh from './langs/kh';
import en from './langs/en';
import cn from './langs/cn';

export default defineI18nConfig(() => ({
    legacy: false,
    messages: {
        kh: kh,
        en: en,
        cn: cn,
    }
}));

