import moment from 'moment';

const functionHelper = {
	dateStringTo12Hour(dateString: string, formatString = 'YYYY-MM-DD') {
		return moment(dateString).format(formatString);
	},
	dateStringTo24Hour(dateString: string, formatString = 'YYYY-MM-DD HH:mm') {
		return moment(dateString).format(formatString);
	},
	dateStringTo12HourWithTime(dateString: string, formatString = 'DD-MM-YYYY hh:mm a'): string {
		return moment(dateString).format(formatString);
	},
	dateStringTo24HourWithTime(dateString: string, formatString = 'DD-MM-YYYY HH:mm'): string {
		return moment(dateString).format(formatString);
	},
	prefixFile(content: string) {
		const base_url = window.location.origin;
		return content?.replace(/uploads/g, base_url + '/uploads');
	},
	getMapDisplay(emebed: string) {
		var regex = /src="([^"]+)"/;
		var match = emebed?.match(regex);
		let googleMapsEmbedUrl = {
			iframe: true,
			link: '',
		};
		if (match && match[1]) {
			googleMapsEmbedUrl.iframe = true;
			googleMapsEmbedUrl.link = match[1];
		} else {
			googleMapsEmbedUrl.iframe = false;
			googleMapsEmbedUrl.link = emebed;
		}
		return googleMapsEmbedUrl;
	}
}
export default functionHelper;