import { useStore } from "~/store";
import { storeToRefs} from "pinia";

export default defineNuxtPlugin((nuxtApp) => {
    const store = useStore();
    const { lang } = storeToRefs(store);

    if (lang.value === 'kh') {
        document.documentElement.classList.add('kh-font')
    } else {
        document.documentElement.classList.remove('kh-font')
    }
})