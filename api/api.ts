import service from "~/utils/request";

// Comment management
export default function useApi() {
	return {
		rgWithGoogle: (params?: object) => {
			return service.post('auth/rgWithGoogle', params);
		},
		rgWithFacbook: (params?: object) => {
			return service.post('rgWithFacbook', params);
		},
		getConfigLogo: (params?: object) => {
			return service({
				url: 'getConfigurationList',
				method: 'get',
				params,
			});
		},
		getCompanyRule: (params?: object) => {
			return service({
				url: 'getCompanyRule',
				method: 'get',
				params,
			});
		},
		getSearchPost: (params?: object) => {
			return service({
				url: 'getSearchPost',
				method: 'get',
				params,
			});
		},
		getConfiguration: (params?: object) => {
			return service.get('getConfiguration', params);
		},
		addConfiguration: (params?: object) => {
			return service.post('saveConfiguration', params);
		},
		updateConfiguration: (params?: object) => {
			return service.post('updateConfiguration', params);
		},
		deleteConfiguration: async (param?: object) => {
			return await service.post('deleteConfiguration', param)
		},
		getAds: (params?: object) => {
			return service({
				url: 'getAdsBanner',
				method: 'get',
				params,
			});
		},
		addAds: (params?: object) => {
			return service.post('saveAds', params);
		},
		deleteAds: (params?: object) => {
			return service.post('deleteAds', params);
		},
		getCategory: (params?: object) => {
			return service({
				url: 'getCategory',
				method: 'get',
				params,
			});
		},
		getCatList: (params?: object) => {
			return service({
				url: 'getCatList',
				method: 'get',
				params,
			});
		},
		getRoom: (params?: object) => {
			return service({
				url: 'getRoom',
				method: 'get',
				params,
			});
		},
		getRoomDisplay: (params?: object) => {
			return service.post('getRoomDisplay', params);
		},
		addOrUpdateRoom: (params?: object) => {
			return service.post('saveRoom', params);
		},
		deleteRoom: (params?: object) => {
			return service.post('deleteRoom', params);
		},
		addOrUpdateCategory: (params?: object) => {
			return service.post('saveCategory', params);
		},
		deleteCategory: (params?: object) => {
			return service.post('deleteCategory', params);
		},
		getBookingRoom: (params?: object) => {
			return service({
				url: 'getBookingRoom',
				method: 'get',
				params,
			});
		},
		getReportByFilter: (params?: object) => {
			return service.post('getReport', params);
		},
		addUpdateBookingRoom: (params?: object) => {
			return service.post('saveBookingRoom', params);
		},
		deleteBookingRoom: (params?: object) => {
			return service.post('deleteBookingRoom', params);
		},
		getHomeStatistics: (params?: object) => {
			return service({
				url: 'getHomeStatistics',
				method: 'get',
				params,
			});
		},
		getBranch: (params?: object) => {
			return service.get('getBranch', params);
		},
		saveUpdateBranch: (params?: object) => {
			return service.post('saveBranch', params);
		},
		deleteBranch: (params?: object) => {
			return service.post('deleteBranch', params);
		},
		getRequestBooking: (params?: object) => {
			return service({
				url: 'getRequestBooking',
				method: 'get',
				params,
			});
		},
		getReadRequestBooking: (params?: object) => {
			return service.get('getCountBooking', params);
		},
		updateIsRead: (params?: object) => {
			return service.post('updateIsRead', params);
		},
		rejectRow: (params?: object) => {
			return service.post('rejectRequestBooking', params);
		},
		saveRequestBooking: (params?: object) => {
			return service.post('saveRequestBooking', params);
		},
		deleteRequestBooking: (params?: object) => {
			return service.post('deleteRequestBooking', params);
		},
		getPostArticle: (params?: object) => {
			return service({
				url: 'getPostArticle',
				method: 'get',
				params,
			});
		},
		getMyPost: (params?: object) => {
			return service({
				url: 'getMyPost',
				method: 'get',
				params,
			});
		},
		saveComment: (params?: object) => {
			return service.post('saveComment', params);
		},
		deleteComment: (params?: object) => {
			return service.post('deleteComment', params);
		},
		savePostArticle: (params?: object) => {
			return service.post('savePost', params);
		},
		deletePostArticle: (params?: object) => {
			return service.post('deletePost', params);
		},
		getBlogArticle: (params?: object) => {
			return service({
				url: 'getBlogArticle',
				method: 'get',
				params,
			});
		},
		saveBlogArticle: (params?: object) => {
			return service.post('saveBlogArticle', params);
		},
		deleteBlogArticle: (params?: object) => {
			return service.post('deleteBlogArticle', params);
		},
		increaseView: (uuid?: object) => {
			return service.post('increaseView', uuid);
		},
		savedPost: (params?: object) => {
			return service.post('savedPost', params);
		},
		getSavedDetail: (params?: object) => {
			return service.post('savedDetail', params);
		},
		getLikeDetail: (params?: object) => {
			return service.post('likeDetail', params);
		},
		saveLike: (params?: object) => {
			return service.post('saveLike', params);
		},
		getLikes: (params?: object) => {
			return service.post('getLikes', params);
		},
		getRelatedPlaces: (params?: object) => {
			return service.post('getRelatedPlaces', params);
		},
		getHomeData: (type?: string) => {
			return service.get(`getDataByCat/${type}`);
		},
		getPostDetail: (post_uuid: string) => {
			return service.get(`getPostDetail/${post_uuid}`);
		},
		getEachPageData: (params: object) => {
			return service({
				url: 'getPostPageType',
				method: 'get',
				params,
			});
		},
		getPlaceByType: (params?: object) => {
			return service({
				url: 'getPlaceByType',
				method: 'get',
				params,
			});
		},
		storeImages(postId: number, items: any[]) {
			return service.post(`saveFiles/${postId}`, items);
		}
	};
}