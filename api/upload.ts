import service from '@/utils/request';

// Comment management
export default function useUpload() {
    return {
        removeFile: (params?: object) => {
            return service.post('removeFile', params);
        },
    };
}
