export default defineNuxtConfig({
  app: {
      head: {
          title: "Tovna24",
          meta: [
              { name: "viewport", content: "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" },
              {
                  hid: "description",
                  name: "description",
                  content: "tovna24.com is a tour place for people who wants to visit all places in Cambodia!",
              },
          ],
          link: [
              { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
              {
                  rel: 'stylesheet',
                  href: 'https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap',
              },
          ],
      },
  },

  site: {
      url: 'https://tovna24.com',
  },

  colorMode: {
      classSuffix: "",
      preference: "light",
      fallback: "light",
  },

  devtools: {
      enabled: true,
      timeline: {
          enabled: true,
      },
  },

  modules: [
      'nuxt-swiper',
      'nuxt-icon',
      "@pinia/nuxt",
      '@pinia-plugin-persistedstate/nuxt',
      "@nuxtjs/i18n",
      "@nuxtjs/color-mode",
      "@nuxtjs/device",
      "@element-plus/nuxt",
      '@nuxtjs/tailwindcss',
      '@nuxtjs/sitemap',
      '@stefanobartoletti/nuxt-social-share',
  ],

  css: ["~/assets/scss/index.scss"],
  components: true,

  i18n: {
      locales: [
          {
              code: 'kh',
              iso: 'kh-KM',
              name: 'ភាសាខ្មែរ',
          },
          {
              code: 'en',
              iso: 'en-US',
              name: 'English',
          },
          {
              code: 'cn',
              iso: 'cn-ZH',
              name: '中文',
          },
      ],
      strategy: "no_prefix",
      defaultLocale: "kh",
      vueI18n: "./i18n.config.ts",
  },

  runtimeConfig: {
      public: {
          baseURL: process.env.NODE_ENV !== 'development' ? 'https://api.tovna24.com/api/' : 'http://127.0.0.1:8000/api/',
          domainUrl: process.env.NODE_ENV !== 'development' ? 'https://tovna24.com' : 'http://localhost:3000',
      },
  },

  nitro: {
      prerender: {
          routes: ["/sitemap.xml"],
      },
  },

  vite: {
      css: {
          preprocessorOptions: {
              scss: {
                  additionalData: `@use "@/assets/scss/element/index.scss" as *;`,
                  // silenceDeprecations: ["legacy-js-api"],
                  api: 'modern-compiler'
              },

          },
      },
      server: {
          // @ts-ignore
          host: '0.0.0.0',
          port: process.env.VITE_PORT ? Number(process.env.VITE_PORT) : 3000,
          open: process.env.VITE_OPEN === 'true',
          hmr: true,
          proxy: {
              '/api': {
                  target: process.env.NODE_ENV !== 'development' ? 'https://api.tovna24.com/api/' : 'http://127.0.0.1:8000/api/',
                  changeOrigin: true,
                  rewrite: (path) => path.replace(/^\/api/, ''),
              },
              '/uploads': {
                  target: process.env.NODE_ENV !== 'development' ? 'https://api.tovna24.com/uploads' : 'https://tovna24.com/uploads',
                  changeOrigin: true,
                  rewrite: (path) => path.replace(/^\/uploads/, ''),
              },
              '/setting/uploads': {
                  target: process.env.NODE_ENV !== 'development' ? 'https://api.tovna24.com' : 'http://127.0.0.1:8000',
                  changeOrigin: true,
                  rewrite: (path) => path.replace(/^\/setting/, ''),
              },
              '/account/uploads': {
                target: process.env.NODE_ENV !== 'development' ? 'https://api.tovna24.com' : 'http://127.0.0.1:8000',
                changeOrigin: true,
                rewrite: (path: string) => path.replace(/^\/account/, ''), //  /account/uploads
              },
              // '/detail/uploads': {
              //   target: process.env.NODE_ENV !== 'development' ? 'https://api.tovna24.com' : 'http://127.0.0.1:8000',
              //   changeOrigin: true,
              //   rewrite: (path: string) => path.replace(/^\/detail/, ''), //  /detail/uploads
              // },
          },
      },
  },

  elementPlus: {
      icon: "ElIcon",
      importStyle: "scss",
      themes: ["dark"],
  },

  compatibilityDate: '2024-12-21',
})