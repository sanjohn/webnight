import axios, { type AxiosInstance } from 'axios';
import { ElMessage, ElMessageBox } from 'element-plus';
import { Session } from '~/utils/storage';
import qs from 'qs';
import { useStore } from '~/store';

//Configure a new axios instance
const service: AxiosInstance = axios.create({
    baseURL: '/api/',
    timeout: 50000,
    headers: { 'Content-Type': 'application/json' },
    paramsSerializer: {
        serialize(params) {
            return qs.stringify(params, { allowDots: true });
        },
    },
});

// Add request interceptor
service.interceptors.request.use(
    (config) => {
        // What to do before sending a request token
        if (Session.get('token')) {
            config.headers!['Authorization'] = `${Session.get('token')}`;
        }
        return config;
    },
    (error) => {
        // What to do about request errors
        return Promise.reject(error);
    }
);
// Add response interceptor
service.interceptors.response.use(
    (response) => {
        // Do something with the response data
        const res = response.data;
        if (res.code && res.code !== 200) {
            // `token` Expired or the account has been logged in elsewhere
            if (res.code === -10004 || res.code === -20004) {
                Session.clear(); // Clear all temporary browser caches
                const route = useRoute();
                let msgTitle = '';
                let msgText = '';
                if (useStore().lang === 'en') {
                    msgTitle = 'Hint';
                    msgText = route.name  === 'detail'? 'Please login!' : 'please login!';
                } else if (useStore().lang === 'kh') {
                    msgTitle = 'ព័ត៌មានជំនួយ';
                    msgText = route.name  === 'detail'? 'សូមចូលគណនី!' : 'សូមចូលគណនី';
                } else {
                    msgTitle = '提示';
                    msgText = '请登录，请登录';
                }
                ElMessageBox.alert(msgText, msgTitle, {})
                    .then(() => {
                     navigateTo('/account/login')   ;
                    })
                    .catch(() => {});
            }else if(res.code === -10001){
                ElMessage.error(res.message);
            }
            // return Promise.reject(service.interceptors.response)
            // ElMessage.error(response.error);
            return response.data;
        } else {
            return res;
        }
    },
    (error) => {
        // Do something about the response error
        if (error.message.indexOf('timeout') != -1) {
            ElMessage.error('network timeout');
        } else if (error.message == 'Network Error') {
            ElMessage.error('Network connection error');
        } else {
            if (error.response.data) ElMessage.error(error.response.statusText);
            else ElMessage.error('Interface path not found');
        }
        return Promise.reject(error);
    }
);

// Export axios instance
export default service;
