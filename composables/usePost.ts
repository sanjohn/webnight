import formHelper from "~/libraries/elementUiHelpers/formHelper";
import useVariable from "~/composables/useVariables";
import { type IPost, Post } from "~/models/IPost";

export default function usePost() {
    const {
        isProcessing,
        api,
        store, t,
        isMobile,
        ruleFormRef,
    } = useVariable();
    const state = reactive({
        form: {
            id: null,
            cat_id: null,
            title: '',
            address: '',
            embed_link: '',
            images: [],
            price: '',
            sort: 9999,
            view: 0,
            type: '',
            place_type: '',
            is_visible: true,
            describe: '',
            uuid: ''
        },
        dialog: {
            title: '',
            submit: '',
            cancel: '',
            isShowDialog: false,
            type: '',
        },
    });
    const resetFields = () => {
        state.form = {
            id: null,
            cat_id: null as any,
            title: '',
            address: '',
            images: [],
            embed_link: '',
            price: '',
            sort: 9999,
            view: 0,
            is_visible: true,
            place_type: '',
            describe: '',
            type: '',
            uuid: ''
        };
    }


    const openDialog = async (type: string, row: object) => {
        if (type === 'edit') {
            console.log(row)
            state.form = Object.assign(state.form, row);
            state.dialog.title = t('edit');
            state.dialog.submit = t('submit');
        } else {
            state.dialog.title = t('post');
            state.dialog.submit = t('submit');
        }
        state.dialog.type = type;
        state.dialog.isShowDialog = true;
    };

    return {
        isProcessing,
        api,
        store, t,
        state,
        openDialog,
        resetFields,
        isMobile,
        formHelper,
        ruleFormRef,
    }
}