
import EnumApiErrorCode from "~/models/enums/enumApiErrorCode";
import useAuthApi from "~/api/auth";

export default function useUserProfile() {
    const api = useAuthApi();
    const state = reactive({
        tableData: {
            data: {},
            loading: false,
        }
    });
    const { openDialogRef, onOpenEditDialog, store, isMobile } = useVariables();
    const userInfo = ref();
    const getTableData = async () => {
        state.tableData.loading = true;
        const response = await api.getInfo();
        if (response.code !== EnumApiErrorCode.success) {
            console.log(response);
        } else {
            state.tableData.data = response.data;
            store.userInfos = {
                userName: response.data.userName,
                profile: response.data.profile,
                phone: response.data.phone
            }
            userInfo.value = response.data;
            state.tableData.loading = false;
        }
    };
    return {
        openDialogRef,
        onOpenEditDialog,
        state,
        isMobile,
        getTableData,
        userInfo,
    }
}