import { onUnmounted, onMounted, reactive } from 'vue';
import { useStore } from '/@/stores/store';

export function useGeolocation() {
  const kordinat = reactive({ latitude: 0, longitude: 0, loading: false })
  const isSupported = 'navigator' in window && 'geolocation' in navigator
  let watcher = null as any;
  const store = useStore();
  onMounted(() => {
    if (isSupported) {
      kordinat.loading = true,
      watcher = navigator.geolocation.watchPosition(
        position => {
          kordinat.latitude = position.coords.latitude;
          kordinat.longitude = position.coords.longitude;
          store.setLatLong(position.coords);
          kordinat.loading = false;
        },
        () => alert('geolocation permission denied')
      )
    } else {
      alert('navigator.geolocation not supported!');
      kordinat.loading = false;
    }
  })

  onUnmounted(() => {
    if (watcher) navigator.geolocation.clearWatch(watcher)
  })
  return { kordinat, isSupported }
}
