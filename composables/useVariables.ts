import { ref } from 'vue';
import type { FormInstance } from 'element-plus';
import { useRoute, useRouter } from 'vue-router';
import useHelperDate from "~/composables/helper/useHelperDate";
import { Local, Session } from '~/utils/storage';
import { useStore } from "~/store";
import useApi from '~/api/api';

const useVariable = () => {
    const { t } = useI18n();
    const isLoading = ref(false);
    const isProcessing = ref(false);
    const ruleFormRef = ref<FormInstance>();
    const dialog = ref(false);
    const route = useRoute();
    const router = useRouter();
    const store = useStore();
    const api = useApi();
    const { isMobile } = useDevice();
    const { currentMonth, shortcutsMonths } = useHelperDate();
    // const { width, height } = useWindowSize();
    const resetForm = (formEl: FormInstance | undefined) => {
        if (!formEl) return
        formEl.resetFields();
    };
    const openDialogRef = ref();
    const onOpenAddDialog = (type: string) => {
        openDialogRef.value.openDialog(type);
    };
    const onOpenEditDialog = (type: string, row: object) => {
        openDialogRef.value.openDialog(type, row);
    };
    const fileList = ref([] as any);
    const renderFunc = ref();
    return {
        t,
        isLoading,
        isProcessing,
        ruleFormRef,
        dialog,
        resetForm,
        openDialogRef,
        fileList,
        onOpenAddDialog,
        onOpenEditDialog,
        renderFunc,
        route, router,
        currentMonth, shortcutsMonths,
        Local, Session,
        store,
        api,
        isMobile,
    };
};

export default useVariable;
