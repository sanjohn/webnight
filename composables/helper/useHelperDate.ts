import { ref } from 'vue';
import moment from 'moment';

interface IDate {
	startDate: string,
	endDate: string,
}
const useHelperDate = () => {
	const currentMonth = ref<IDate>({
		startDate: moment().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
		endDate: moment().endOf('month').format('YYYY-MM-DD HH:mm:ss'),
	});
	const { t } = useI18n();
	const shortcutsMonths = [
		{
			text: t('thisMonth'),
			value: [moment().startOf('month').format('YYYY-MM-DD HH:mm:ss'), moment().endOf('month').format('YYYY-MM-DD HH:mm:ss')],
		},
		{
			text: t('lastMonth'),
			value: [moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD HH:mm:ss'), moment().subtract(1, 'months').endOf('month').format('YYYY-MM-DD HH:mm:ss')]
		},
		{
			text: t('last2Months'),
			value: [moment().subtract(2, 'months').startOf('month').format('YYYY-MM-DD HH:mm:ss'), moment().subtract(2, 'months').endOf('month').format('YYYY-MM-DD HH:mm:ss')],
		},
		{
			text: t('last3Months'),
			value: [moment().subtract(3, 'months').startOf('month').format('YYYY-MM-DD HH:mm:ss'), moment().subtract(3, 'months').endOf('month').format('YYYY-MM-DD HH:mm:ss')],
		},
	];
	const currentDay = ref(moment().format('DD-MM-YYYY HH:mm a'));
	const currentTimeWithSecond = ref(moment().format('LTS'));
	return {
		currentMonth,
		shortcutsMonths,
		currentDay,
		currentTimeWithSecond,
	};
};

export default useHelperDate;
