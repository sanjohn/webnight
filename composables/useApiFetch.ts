import type ApiResponse from "~/models/IApiResponse";

export const useApiFetch = () => {
  const config = useRuntimeConfig();
  const get = async (request?: string): Promise<any> => {
    const response = await useFetch(`${config.public.baseURL + request}`, {
      method: 'GET',
    });
    return response;
  };
  return {
    get,
  }
}