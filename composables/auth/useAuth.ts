import formHelper, { type IRule } from "~/libraries/elementUiHelpers/formHelper";
import useVariable from "~/composables/useVariables";
import useAuthApi from "~/api/auth";
import EnumApiErrorCode from "~/models/enums/enumApiErrorCode";
import { messageNotification } from "~/libraries/elementUiHelpers/notificationHelper";
import EnumMessageType from "~/models/enums/enumMessageType";
import Cookies from "js-cookie";
import type { IAuth } from '~/models/IUser';
import service from "~/utils/request";

export default function useAuth() {
    const api = useAuthApi();
    const state = reactive({
        formLogin: {
            usernameOrEmail: '',
            password: '',
        },
        formRegister: {
            username: '',
            phone: '',
            email: '',
            password: '',
            confirm_password: '',
        },
    });
    const {
        isProcessing,
        ruleFormRef,
        t, store,
        router
    } = useVariable();
    const validateUsername = (): string => {
        if (!state.formRegister.username) {
            return t('usernameRequire');
        }
        if (/\s/.test(state.formRegister.username)) {
            return t('white_space_not_allow');
        }
        return '';
    };

    const validateEmail = (): string => {
        if (/\s/.test(state.formRegister.email)) {
            return t('white_space_not_allow');
        }
        const regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if (!(regexp.test(state.formRegister.email.toLowerCase()))) {
            return t('invalid_email');
        }
        return '';
    };
    const validatePassword = (): string => {
        if (/\s/.test(state.formRegister.password)) {
            return t('white_space_not_allow');
        }
        return '';
    };
    const validatePasswordConfirmation = (): string => {
        if (/\s/.test(state.formRegister.confirm_password)) {
            return t('white_space_not_allow');
        }
        return '';
    };
    const rules: Record<string, IRule> = ({
        usernameOrEmail: { required: true },
        password: { required: true },
    });

    const loginRules = formHelper.getRules(rules);
    const login = async () => {
        isProcessing.value = true;
        const request = {
            usernameOrEmail: state.formLogin.usernameOrEmail,
            password: state.formLogin.password,
        };
        try {
            const response = await api.login(request);
            if (response.code !== EnumApiErrorCode.success) {
                messageNotification(t(response.message), EnumMessageType.Error);
            } else {
                router.back()
                messageNotification(t('success'), EnumMessageType.Success);
                authFunc(response.data);
            }
        } catch (e) {
            console.log(e);
            isProcessing.value = false;
        }
        isProcessing.value = false;
    };
    const onLogin = formHelper.getSubmitFunction(login);
    const registerFunc = async () => {
        try {
            isProcessing.value = true;
            const request = {
                userName: state.formRegister.username,
                phone: state.formRegister.phone,
                email: state.formRegister.email,
                password: state.formRegister.password,
            }
            const response = await api.saveUser(request);
            if (response.code !== EnumApiErrorCode.success) {
                messageNotification(t(response.message), EnumMessageType.Error);
            } else {
                navigateTo('/account/login');
                messageNotification(t('success'), EnumMessageType.Success);
            }
        } catch (e) {
            isProcessing.value = false;
        }
    };

    const rulesRegister: Record<string, IRule> = ({
        username: { customRule: validateUsername, required: true },
        phone: { required: true },
        email: { customRule: validateEmail, required: true },
        password: { customRule: validatePassword, required: true },
        confirm_password: { customRule: validatePasswordConfirmation, required: true },

    });
    const registerRules = formHelper.getRules(rulesRegister);
    const onRegister = formHelper.getSubmitFunction(registerFunc);

    const setCookieWithExpiration = (cookieName: string, cookieValue: string, expirationInMinutes: number, onExpirationCallback: any) => {
        // Set the cookie with expiration
        Cookies.set(cookieName, cookieValue, { expires: expirationInMinutes / (24 * 60) }); // Convert minutes to days
        setTimeout(function () {
            // Execute the callback function
            if (typeof onExpirationCallback === 'function') {
                onExpirationCallback();
            }
        }, expirationInMinutes * 60 * 1000); // Convert minutes to milliseconds
    }

    // Function to refresh token and set cookie again
    const refreshTokenAndSetCookie = async () => {
        try {
            const response = await api.refresh();
            if (response.code !== EnumApiErrorCode.success) {
                messageNotification(response.message, EnumMessageType.Error);
            } else {
                authFunc(response.data);
                service.defaults.headers['Authorization'] = response.data.access_token;
                setCookieWithExpiration('refreshTime', '20mns', 20, refreshTokenAndSetCookie);
            }
        } catch (e) {
            return console.log(e);
        }
    };
    // ✅ Add 1 hour to the current date
    const addHours = (date: Date, access_time: number) => {
        date.setTime(date.getTime() + access_time * 1000);
        return date;
    }
    const authFunc = (param: IAuth) => {
        //todo minus 10secs
        const accessToken = addHours(new Date(), param.expires_in) as any;
        Session.set('token', param.access_token);
        Session.set('userName', param.userInfo['userName']);
        store.updateUserInfo(param.userInfo);
    };
    return {
        state,
        loginRules,
        registerRules,
        ruleFormRef,
        isProcessing,
        onLogin,
        onRegister,
        authFunc,
        setCookieWithExpiration,
        refreshTokenAndSetCookie,
    }
}