import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider, FacebookAuthProvider } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyDoTMl5bHnoJHoZ3Aio04sC0cbp3aAaPfw",
    authDomain: "poipet-fb.firebaseapp.com",
    projectId: "poipet-fb",
    storageBucket: "poipet-fb.appspot.com",
    messagingSenderId: "219963874051",
    appId: "1:219963874051:web:e8638f117dc13d3a09e736",
    measurementId: "G-WLJDBB872P"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const facebookProvider = new FacebookAuthProvider();
const googleProvider = new GoogleAuthProvider();
export { auth, facebookProvider, googleProvider }


