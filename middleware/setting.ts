
export default defineNuxtRouteMiddleware((to, from) => {
    const token = computed(() => process.client ? Session.get('token') : null);
    if (process.client) {
        if (to.path === '/setting' || (token.value && to.path === '/setting')) {
            return navigateTo('/setting/mypost');
        };
        if (!token.value && to.path === '/setting') {
            // If there's no token, clear the session and redirect to the login page
            Session.clear();
            return navigateTo('/account/login');
        };
        if (!token.value && ['/setting/myprofile', '/setting/mypost', '/setting/admin', '/setting/mysave'].includes(to.path)) {
            // If there's no token, clear the session and redirect to the login page
            Session.clear();
            return navigateTo('/account/login');
        };
    }
});