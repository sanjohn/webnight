import { useStore } from '~/store';

export default defineNuxtRouteMiddleware((to, from) => {
    const token = computed(() => process.client ? Session.get('token') : null);
    if (process.client) {
        if (to.path === '/account/login' && !token.value) {
            return; // No need to redirect, already on the login page without a token
        }
        if (!token.value && to.path === '/setting') {
            // If there's no token, clear the session and redirect to the login page
            Session.clear();
            return navigateTo('/account/login');
        }
        if (token.value && (to.path === '/account/login' || to.path === '/account/register')) {
            // If there's a token and the user tries to access login or register, redirect to setting
            window.location.href = '/setting'
        }
    }
});